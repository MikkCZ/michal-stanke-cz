---
layout: post
title: Výměna servomotoru pátých dveří u Škody Octavia / Replacing electric trunk struts of Skoda Octavia
tags:
  - czech
  - english
---

[_english version below_](#en)

Dva roky jezdím Škodou Octávií kombi 3. generace. Kupoval jsem ji jako ojetou, ale v relativně slušné výbavě, která obnášela i elektricky ovládané páté dveře. Pořád to považuji za šikovnou věc. Ale... První porucha přišla po roce, kdy se víko pátých dveří nechtělo pořádně otevřít a následně ani zavřít. Veškeré pokusy končily úpěnlivým pípáním, a bylo potřeba velmi jemné pošťouchnutí. Podezírám zde blíže neurčeného člena rodiny, který párkrát za víko vzal a "pomohl" mu při otevírání, a následně ho zavíral také ručně. Výsledek byla zničená levá vzpěra/servomotor. Protože se nepodařilo sehnat z vrakoviště funkční náhradu, výměna v neznačkovém servisu ale za originální díl vyšla na 9 tisíc.

Nyní po dalším roce se začala ozývat i vzpěra pravá, která vypadala funkčně, ale bylo už před rokem jasné, že ji čeká podobný osud. Ozývat se začala doslova, a to hlasitým lupnutím při zavírání. Podle čísla dílu vyznačeném na vzpěře jsem zjistil, že originální díl samotný vyjde na víc než 9 tisíc, a neoriginání díly nebo použité z druhé ruky na 4 až 5 tisíc. Použitý jsem rozhodně nechtěl, a u neoriginálních jsem měl oprávněný strach, že dohromady s druhou originální vzpěrou si nebudou rozumět. Nakonec jsem se našel na Amazonu [neoriginální sadu dvou vzpěr](https://www.amazon.de/-/cs/gp/product/B094JBH581/){:target="_blank"} v přepočtu za necelých 6 tisíc včetně dopravy.

Balíček přišel od objednání za 3 pracovní dny. Vzpěry byly zabalené v polystyrenových obalech, na kterých byl nalepený velmi strohý seznam instrukcí. Vzpěry samotné nejsou nijak popsaná, kromě malého lístečku která patří vpravo a vlevo. Na první pohled mnohem masivnější/tlustší než originální, ale nedělám si iluze, že jakkoliv bytelnější. Místo měkkého husího krčku jsou kabely vedené v tužší gumě, což je vidět už na fotkách na Amazonu.

[![porovnání vzpěr](/assets/img/octavia-struts.jpg){:style="max-height:250px"}](/assets/img/octavia-struts.jpg){:style="display:block;text-align:center"}

Na YouTube jsem si našel návod na výměnu, a přišla mi dostatečně jednoduchá, abych se o ni pokusil sám.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/toYYxcLvA6w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin-bottom: 1em;"></iframe>

Pár poznatků z výměny:
- Sundat plasty v interiéru není vůbec problém a nebylo potřeba se vůbec bát použít trochu síly. Správné vrácení bylo těžší.
- Smeták pro podepření víka je potřeba mít spíš delší, alespoň 1,5 metru.
- Sponám, které vzpěry drží, se moc nechce zpátky. Pořádné zkontrolujte, že s novou vzpěrou zapadly úplně a že vzpěra na drží opravdu pevně.
- Zkusil jsem nejdřív vyměnit vzpěru jenom jednu a potvrdilo se mi, že si se s druhou originální přetlačuje a víko nefunguje. Recenze u samostatně prodávaných kusů stejné značky to potvrzují. Určitě tedy měňte vzpěry za stejné.

Nakonec je podle strohých informací u koupených vzpěr potřeba víko velmi jemně ručně zavřít, aby se spolu srovnaly, a pak vše funguje. A když už budete u té výměny, můžete si velmi jednoduše rovnou [nastavit, jak vysoko se má víko kufru otevírat](https://www.youtube.com/watch?v=kyHW-xeSEuY){:target="_blank"}.

Závěrem jediný rozdíl v porovnání s originálními vzpěrami z pohledu fungování je, že když při otevírání povolí zámek, se víko prvních 10 - 20 centimetrů pootevře opravdu rychle, a pak zpomalí na rychlost jako u originálu. V recenzích někteří píší, že jsou tyhle vzpěry pomalejší a tišší, ale já takový pocit nemám.

# English
{:#en}

For two years I am driving 3rd generation of Skoda Octavia combi. I bought it as a used car with relatively rich equipment including electric trunk. I still consider it a nice bonus, but... It first got broken after a year, when the trunk won't open or close. All attempts were ending with a long warning beep and the trunk stopping half way. I suspect an unnamed family member, who several times "helped" the trunk forcibly open and close with their hands. The result was a destruction of the left strut servo-motor. After the first unsuccessful attempt to get a replacement from a wreck yard, the bill was ~9000 CZK (~370 EUR) for an original part in an unbranded car service shop.

Now, one year later, the second strut started to make itself known by cracking noises when the trunk was closing. It looked ok the year before, but it was clear the whole time it's only a matter of time when it will give up as well. Using the part number on the strut I found out the price of a new original part, which was above 9000 CZK, just for the part. A used one, or a secondary production, would cost about 4000 or 5000 CZK (between 160 and 210 €). But I did not want another used strut in a questionable shape, and with the secondary production I didn't know whether it will work together with the second original strut. At the end I found a whole [set of secondary production struts on Amazon](https://www.amazon.de/-/en/gp/product/B094JBH581/){:target="_blank"} for 240 € including delivery.

The package came 3 working days after I placed the order. The struts protected in a polystyren cases, with a very brief instructions list. The struts do not have any brand or details on them, except a small ticket which one is left and which one is right. At first sight, the secondary production struts are more massive than the original ones, but I double they would be any more durable. Instead of a soft gooseneck, their cables are inside a more thick rubber, which you can actually see from the pictures on Amazon.

[![struts comparison](/assets/img/octavia-struts.jpg){:style="max-height:250px"}](/assets/img/octavia-struts.jpg){:style="display:block;text-align:center"}

I looked up on YouTube how to replace the struts, and it looked simple enough for me to try myself.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/toYYxcLvA6w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="margin-bottom: 1em;"></iframe>
Several personal observations from the replacement:
- Removing the plastic interior covers is quite simple, once you apply enough force. Getting them back is more delicate and requires some precision.
- The broom or stick to support the rear door open has to be quite long actually, at least 1,5 meters.
- Fitting back the brackets holding the strut requires some precision. Double check the bracket are fully in their place and that the strut actually holds.
- I tried replacing a single strut first, and I confirmed it wouldn't work with a second original one. They apply difference force so the door stops with a warning beep. The reviews of individually sold struts of the same brand actually say the same. So make sure when replacing a strut that both are the same.

To finish the replacement procedure, according to the brief instructions that came with the struts I had to close the door gently with my hands for calibration, and then everything works. When replacing the struts, you might also take the opportunity to easily [adjust the position you want the door to open](https://www.youtube.com/watch?v=kyHW-xeSEuY){:target="_blank"}.

At the end, the only difference of the new struts in terms of how they work is, that when opening the door, once it unlocks, the first 10 or 20 centimeters open quite fast, then the door slows down to the speed of the original struts. In review several people mention these struts are slower and quieter, but I do not have such feeling.