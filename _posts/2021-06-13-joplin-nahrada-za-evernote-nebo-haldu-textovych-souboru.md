---
layout: post
title: "Joplin: náhrada za Evernote nebo haldu textových souborů"
tags: czech
---

Dobře, s tím Evernotem si vymýšlím, protože jsem ho nikdy nepoužíval, ale stejně si myslím, že je Joplin docela dobrá aplikace na poznámky a měli byste ji vyzkoušet. Já sám jsem si dlouho psal (a někdy pořád píšu) poznámky na kusy papíru, které se mi potom válí různě po stole. Podle toho, jak jednotlivé věci dokončuji, nebo se k nim dlouho nedostanu, až přestanou být zase relevantní, pak papírky proškrtávám a zase přepisuju na nové. Tenhle zlozvyk jsem si bohužel přenesl i do poznámek psaných na počítači, který mi brzy zahltila spousta náhodných a často nepojmenovaných textových souborů psaných v geditu, které jsem si stejně jako papíry na stole nedokázal nijak zorganizovat.

Jako první jsem začal poznámky nějak třídit v [experimentálním Firefoxu Notes](https://www.mozilla.cz/zpravicky/jake-funkce-dostane-experiment-notes/), ale jeho aplikace pro Android nebyla vždy úplně spolehlivá a udržovaná a bohužel jsem ji nevydržel používat ani do oficiálního ukončení projektu. Vnést řád do texťáků se mi nepodařilo, takže jsem je nakonec smazal a letošní rok začal načisto s Joplinem.

## Co je Joplin a jak vypadá
Joplin, [jak autoři popisují](https://joplinapp.org/), je open-source aplikace pro psaní poznámek a seznamů úkolů, která nabízí i určité možnosti jejich synchronizace. Dlouho jsem si myslel, že se aplikace jmenuje podle zpěvačky [Janis Joplin](https://cs.wikipedia.org/wiki/Janis_Joplin), ale často kladené otázky projektu mi prozradily, že je to ve skutečnosti podle hudebníka [Scotta Joplina](https://cs.wikipedia.org/wiki/Scott_Joplin).

Zpátky k tématu, Joplin je docela jednoduchá aplikace, kterou můžete spustit a začít používat. Na počítači s Linuxem, Windows a macOS máte výběr mezi variantou s grafickým rozhraním nebo jako aplikaci do terminálu, pro mobily s Androidem nebo iOS pak samozřejmě jenom s grafickým rozhraním. Pro svoje osobní poznámky používám grafickou variantu na Fedoře instalovanou přes Flatpak a aplikace pro Android, v práci pak na macOS instalováno z Homebrew. I když Flatpak ani Homebrew nejsou oficiálně podporované, všechno, co používám, funguje bez problémů.

[![Joplin zařízení](https://joplinapp.org/images/AllClients.jpg){:style="max-height:250px"}](https://joplinapp.org/images/AllClients.jpg){:style="display:block;text-align:center"}

Rozhraní aplikace pro počítač se skládá ze dvou postranních lišt a editoru poznámek. Jejich rozložení, byť ne moc intuitivním způsobem, si můžete přeskupit. V první listě poznámky organizujete do hierarchie zápisníků, tedy něco jako složky, a přidáváte jim štítky. Ve druhé liště v právě vybraném zápisníku procházíte a zakládáte jednotlivé poznámky a seznamy úkolů.

[![Joplin na počítači](/assets/img/joplin-desktop.png){:style="max-height:250px"}](/assets/img/joplin-desktop.png){:style="display:block;text-align:center"}

Editor umí tři režimy - čistě [WYSIWYG](https://cs.wikipedia.org/wiki/WYSIWYG), [Markdown](https://cs.wikipedia.org/wiki/Markdown) a nakonec kombinace Markdownu a vedle zobrazeného vykresleného výsledku. Alternativně jde poznámku z Joplinu otevřít do výchozího nebo vámi nastaveného editoru textových souborů a Markdown upravit v něm. Z možností Markdownu pak vychází možnosti formátování poznámek, takže můžete používat věci jako nadpisy, seznamy, odkazy a samozřejmě i text psaný tučně nebo kurzívou. Zatím je pro mě Joplin taky nejlepší aplikací pro export Markdownu do PDF, a v tomto ohledu určitě spolehlivější než třeba Atom. Obsah tohohle článku včetně formátování a odkazů jsem bez problémů připravil právě v Joplinu. Párkrát už mě ale zamrzela absence formátování velikost písma nebo barevného zvýraznění.

Na obrazovku telefonu se samozřejmě tohle všechno vedle sebe nevejde, takže po spuštění uvidíte seznam poznámek ve posledním otevřeném zápisníku, který můžete změnit skrze nabídku po levé stráně, a po klepnutí uvidíte samotnou poznámku, kterou můžete dalším klepnutím na tlačítko upravit.

[![Zápisníky - Android](/assets/img/joplin-android-collections.png){:style="max-height:340px"}](/assets/img/joplin-android-collections.png)
[![Poznámka - Android](/assets/img/joplin-android-note.png){:style="max-height:340px"}](/assets/img/joplin-android-note.png)

## Nastavení aplikace
Můžete si upravit jazyk rozhraní aplikace (čeština je dostupná, byť ne zcela kompletní), formáty data a času (škoda, že se neberou automaticky z nastavené lokalizace), kontrolu pravopisu, barevný vzhled rozhraní, chování při vytváření nové poznámky, písmo editoru, ale i některé další funkce samotného Markdownu. Já nicméně Joplin používám prakticky ve výchozím nastavení.

Jsou tu i možnosti nastavení zmiňované synchronizace a šifrování dat. Na počítači umí Joplin i několik rozšíření.

## Synchronizace a šifrování dat
Hlavní funkcí, co jsem od poznámek chtěl a kterou mi Firefox Notes tehdy dal, je synchronizace mezi počítačem a telefonem. Joplin umí poznámky ukládat:
- na souborový systém,
- do úložiště AWS S3,
- do Dropboxu, OneDrivu a NextCloudu,
- kamkoliv přes WebDAV,
- Joplin server s podporou sdílení poznámek.

Už první možnost ukládat data do adresáře vám zajistí určité možnosti synchronizace. Můžete totiž klidně vybrat adresář synchronizovaný cloudovou službou podle svého výběru. Já mám takhle pracovní poznámky synchronizované skrze Box.com, který jinak není přímo podporovaný. Stejně to bude fungovat i s Diskem Google a dalšími. Pro synchronizaci osobních poznámek používám NextCloud. Přímou integraci se mi z nějakého důvodu rozchodit nepodařilo, ale přes WebDAV funguje dobře.

Při synchronizaci skrz úložiště, které nemáte zcela pod kontrolou, nebo mu z jiného důvodu pro své poznámky úplně nedůvěřujete, můžete použít [koncové šifrování](https://cs.wikipedia.org/wiki/Koncov%C3%A9_%C5%A1ifrov%C3%A1n%C3%AD), kdy jsou data v čitelné podobě vidět jenom přímo v aplikaci. V úložišti i při komunikaci s ním jsou zašifrovaná tak, aby je nikdo jiný dešifrovat ani nemohl. Klíč, kterým se šifruje, je také synchronizovaný, a je chráněný heslem. Dokumentace doporučuje šifrování zapnout nejdřív na jednom zařízení, nejlépe na počítači, poté počkat na dokončení synchronizace všech dat, a následně spustit synchronizaci v ostatních aplikacích a na vyžádání zadat heslo ke klíči. Aplikace si pro vás heslo pamatuje, rizikem koncového šifrování ale obecně je, že tak, jako se nikdo k vašim datům bez klíče a hesla nedostane, tak ani vy v případě ztráty všech klientů a zapomenutí hesla už svoje poznámky nikdy neuvidíte.

V souborech uložených Joplinem se ale ani bez šifrování ručně přehrabovat chtít nebudete. Aby mohl udržovat historii změn a taky řešit případné kolize během synchronizace, neukládá Joplin celé poznámky do jednoho souboru, ale místo toho má každou poznámky v mnoha souborech se změnami a přírůstky.

## Rozšíření
Joplin má i oficiální [repositář s rozšířeními](https://github.com/joplin/plugins), kterých je v době psaní tohoto textu skoro 40. Najdete tam rozšíření např. pro:
- vytvoření nové poznámky z označeného textu,
- export zápisníku do generátoru statických webových stránek (Hugo, Gatsby, Jekyll),
- seznam oblíbených poznámek,
- zobrazení stavu odkazovaných ticketů v systému Jira,
- provádění výpočtů,
- otevírání poznámek v panelech.

Žádné z uvedených mě ale nabízenými funkcemi buď neoslovilo, nebo by mi nestačilo ho mít jen na počítači. Aplikace pro Android rozšíření nepodporuje, nebo jsme je tam minimálně nenašel.

## Co bych v Joplinu ještě rád viděl
Od začátku roku mi zatím v Joplinu nic vyloženě nechybělo. Uvítal bych trochu širší možnosti formátování textu a někomu zase může chybět možnost upravovat poznámky odkudkoliv, takže webové rozhraní. Ještě jsem nezkoumal výše zmiňovaný Joplin server, a oficiální stránky projektu o něm zase tolik napíší, takže možná jednou...

Pro mě je Joplin vlastně první aplikace svého druhu. Jsem s ní spokojený, ale chybí mi srovnání s konkurencí. Ať už aplikaci na psaní poznámek používáte, nebo si myslíte, že by se vám nějaká pro organizací práce a myšlenek hodila, zkuste prosím Joplin a podělte se o svůj názor, jak vyhovuje vám.
