---
layout: page
title: About me
permalink: /
---

![avatar](https://www.gravatar.com/avatar/85c8761c320031003a2eaa28f49dc557?s=96){:style="display:block;margin:0 auto;border-radius:15px"}

My name is **Michal Stanke**. I am a software engineer, member of [OpenAlt](https://www.openalt.org/o-spolku) and a [Mozillian](https://people.mozilla.org/p/mstanke) from the Czech Republic. For the most part I am involved in localization and community activities around [Mozilla.cz](https://www.mozilla.cz/).
